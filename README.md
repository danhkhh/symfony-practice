## Set up
+ Clone this repo and place it in the web root directory.
+ Create file app\config\parameters.yml from file app\config\parameters.yml.dist
For example my parameters.yml
```
parameters:
    database_host: 127.0.0.1
    database_port: 3306
    database_name: testsym
    database_user: root
    database_password: null
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: 3414e33d96d81e91f53d12972ea6743d1850f3ff
```
+ Run below commands one by one to prepare database:
```
php bin/console doctrine:database:create
php bin/console doctrine:generate:entities AppBundle/Entity/Role
php bin/console doctrine:generate:entities AppBundle/Entity/User
php bin/console doctrine:schema:update --force
```

+ Access this url for user page: http://localhost/symfony-practice/web/user
The screen may show no user. Please don't forget to add new users before you can edit/delete the users.

## Notes
+ Vendor is included so no need to run "composer install"
+ PHP 7.3 requires doctrine/orm version 2.6.3 or above. I am using PHP 7.3 with doctrine/orm version 2.5.14 so I receive this message
```
Warning: "continue" targeting switch is equivalent to "break". Did you mean to use "continue 2"?
```    
Updating doctrine/orm with company internet connection is annoying, so I edited file vendor\doctrine\orm\lib\Doctrine\ORM\UnitOfWork.php as a quick temporary fix.
