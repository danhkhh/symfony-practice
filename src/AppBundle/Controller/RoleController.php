<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RoleController extends BaseController
{
    /**
     *
     * @return Response|void
     */
    public function indexAction(Request $request)
    {
        $roles = $this->getAllCurrentRoles();
        while (empty($roles)) {
            $this->seedRoleData();
            $roles = $this->getAllCurrentRoles();
        };
        return $this->createApiResponse($roles);
    }

    /**
     *
     * @return array
     */
    private function getAllCurrentRoles()
    {
        $roles = $this->getDoctrine()
            ->getRepository('AppBundle:Role')
            ->findAll();
        return $roles;
    }

    /**
     *
     */
    private function seedRoleData()
    {
        $doctrineManager = $this->getDoctrine()->getManager();
        $roleNames = ['Guest', 'Call center', 'Admin'];
        foreach ($roleNames as $roleName) {
            $currentRole = new Role();
            $currentRole->setName($roleName);

            $doctrineManager->persist($currentRole);
        }
        $doctrineManager->flush();
    }
}
