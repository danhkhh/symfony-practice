<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use AppBundle\Entity\User;

class UserController extends BaseController
{
    /**
     *
     * @return Response
     * @throws \Exception
     */
    public function adminAction()
    {
        return $this->render('user/admin.html.twig', []);
    }

    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();
        return $this->createApiResponse($users);
    }

    public function viewAction($id)
    {
        $user = $this->loadEntity($id);
        return $this->createApiResponse(['success' => true, 'data' => $user]);
    }

    public function createAction(Request $request)
    {
        $user = new User();
        return $this->processPostData($request, $user);
    }

    public function updateAction(Request $request, $id)
    {
        $user = $this->loadEntity($id);
        return $this->processPostData($request, $user, ['method' => 'PUT']);
    }

    public function deleteAction($id)
    {
        try {
            $user = $this->loadEntity($id);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
            return $this->createApiResponse(['success' => true]);
        } catch (\Exception $e) {
            return $this->createApiResponse(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    private function loadEntity($id)
    {
        $entity = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        if (!$entity) {
            throw $this->createNotFoundException(
                'No item found for id '.$id
            );
        }
        return $entity;
    }

    private function processPostData(Request $request, $user, $formOptions = [])
    {
        $content = $request->getContent();
        $content = json_decode($content, true);
        $request->request->replace(['user' => $content]);


        $form = $this->createForm(UserType::class, $user, $formOptions);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // $form->getData() holds the submitted values
                // but, the original `$task` variable has also been updated
                //$user = $form->getData();

                $role = $this->getDoctrine()
                    ->getRepository(Role::class)
                    ->find($user->getRoleId());
                $user->setRole($role);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                return $this->createApiResponse(['success' => true, 'data' =>$user]);
            } else {
                return $this->createApiResponse(['success' => false]);
            }
        } else {
            throw new AccessDeniedHttpException("Your request is invalid.");
        }
    }
}
