app.controller('UserController', function($scope, UserService) {
    $scope.users = [];
    $scope.currentUser = null;
    $scope.currentIndex = null;
    $scope.roles = [];
    getUserList();
    getRoleList();

    function getRoleList()
    {
        UserService.getAllRoles().then(
            function (response) {
                $scope.roles = response.data;
                console.log("Roles");
                console.log(response);
            },
            function() {
                alertError();
            });
    }

    function getUserList()
    {
        UserService.getAllItems().then(function (response) {
                console.log("Users");
                console.log(response);
                $scope.users = response.data;
            }, function() {
                alertError();
            });
    }

    $scope.submitForm = function()
    {
        let userData = _.pick($scope.currentUser, ['id', 'firstname', 'lastname', 'address', 'phone', 'gender', 'roleId']);
        if ($scope.currentUser.hasOwnProperty('id')) {
            UserService.updateItem(userData).then(function(response) {
                console.log("Submit response");
                console.log(response);
                if (response.data.success) {
                    $scope.users[$scope.currentIndex] = $scope.currentUser;
                    $scope.closeUserForm();
                } else {
                    alertError();
                }
            }, function() {
                alertError();
            });
        } else {
            UserService.createItem(userData).then(function(response) {
                console.log("Submit response");
                console.log(response);
                if (response.data.success) {
                    $scope.users.push(response.data.data);
                    $scope.closeUserForm();
                } else {
                    alertError();
                }
            }, function() {
                alertError();
            });
        }
    };

    $scope.cancelForm = function()
    {
        if ($scope.userForm.$dirty) {
            if (confirm("The user is not created yet. Let's stay to finish.")) {
                return;
            }
        }
        $scope.closeUserForm();
    };

    $scope.add = function()
    {
        $scope.currentUser = {};
        $scope.userForm.$setPristine();
        $scope.userForm.$setUntouched();
        $scope.openUserForm();
    };

    $scope.edit = function(user, index)
    {
        $scope.currentUser = JSON.parse(JSON.stringify(user));
        $scope.currentIndex = index;
        $scope.openUserForm();
    };

    $scope.delete = function(id, index)
    {
        if (confirm("Are you sure that you want to delete this user?")) {
            UserService.deleteItem(id).then(function(response) {
                if (response.data.success) {
                    $scope.users.splice(index, 1);
                } else {
                    alertError();
                }
            }, function () {
                alertError();
            });
        }
    };

    $scope.openUserForm = function()
    {
        $('#modal-user-form').modal("show");
    };

    $scope.closeUserForm = function()
    {
        $('#modal-user-form').modal("hide");
    };

    function alertError()
    {
        alert("Sorry. There is an error with your request.");
    }
});
