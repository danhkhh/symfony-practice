app.service("UserService", function($http) {
    var url = 'api/users';

    // get all items
    this.getAllRoles = function() {
        return $http.get('api/roles');
    };

    // get all items
    this.getAllItems = function() {
        return $http.get(url);
    };

    // add new item
    this.createItem = function(itemData) {
        return $http.post(url, JSON.stringify(itemData), {headers: {'Content-Type': 'application/json'}});
        /*return $http({
            method: "post",
            url: "Student/AddStudent",
            data: JSON.stringify(itemData),
            dataType: "json"
        });*/
    };

    // Update item
    this.updateItem = function(itemData) {
        let id = itemData.id;
        delete itemData['id'];
        return $http.put(url + "/" + id, JSON.stringify(itemData));
        //return $http.put(url + "/" + itemData.id, JSON.stringify(itemData));
    };

    // Delete item
    this.deleteItem = function(id) {
        return $http.delete(url + "/" + id);
        return $http.post('Student/DeleteStudent/' + id)
    }
});
